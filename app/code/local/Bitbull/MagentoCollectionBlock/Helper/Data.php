<?php
/**
 * @category Bitbull
 * @package  Bitbull_MagentoCollectionBlock
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_MagentoCollectionBlock_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * Add filter in collection
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $name
     * @param $detail
     * @return \Mage_Catalog_Model_Resource_Product_Collection
     */
    public function addAttributeFilter($collection, $name, $detail){
        switch ($detail['type'] ){
//            case 'yesno':
//                //TODO: testare il yes
//                $collection->addAttributeToFilter($name , array('Yes' => $detail['value']));
//                break;
            default :
                $collection->addAttributeToFilter( $name, $detail['value']);
                break;
        }

        return $collection;
    }

    /**
     * Add attribute to select in collection
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $attributes
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function addAttributesToSelect($collection, $attributes){
        return $collection->addAttributeToSelect($attributes);
    }


    /**
     * Imported method of the reports/product_collection class without limit on the product type
     *
     * removed type_id NOT IN ('grouped', 'configurable', 'bundle') to retrieve all type of product
     * @param $collection
     * @return mixed
     */
    public function addOrderedQty($collection)
    {
        $adapter              = $collection->getConnection();
        $orderTableAliasName  = $adapter->quoteIdentifier('order');

        $orderJoinCondition   = array(
            $orderTableAliasName . '.entity_id = order_items.order_id',
            $adapter->quoteInto("{$orderTableAliasName}.state <> ?", Mage_Sales_Model_Order::STATE_CANCELED),

        );

        $productJoinCondition = array(
            'e.entity_id = order_items.product_id',
            $adapter->quoteInto('e.entity_type_id = ?', $collection->getProductEntityTypeId())
        );

        $collection->getSelect()->reset()
            ->from(
                array('order_items' => $collection->getTable('sales/order_item')),
                array(
                    'ordered_qty' => 'SUM(order_items.qty_ordered)',
                    'order_items_name' => 'order_items.name'
                ))
            ->joinInner(
                array('order' => $collection->getTable('sales/order')),
                implode(' AND ', $orderJoinCondition),
                array())
            ->joinLeft(
                array('e' => $collection->getProductEntityTableName()),
                implode(' AND ', $productJoinCondition),
                array(
                    'entity_id' => 'order_items.product_id',
                    'entity_type_id' => 'e.entity_type_id',
                    'attribute_set_id' => 'e.attribute_set_id',
                    'type_id' => 'e.type_id',
                    'sku' => 'e.sku',
                    'has_options' => 'e.has_options',
                    'required_options' => 'e.required_options',
                    'created_at' => 'e.created_at',
                    'updated_at' => 'e.updated_at'
                ))
            ->where('parent_item_id IS NULL')
            ->group('order_items.product_id')
            ->having('SUM(order_items.qty_ordered) > ?', 0);
        return $collection;
    }

    public function getNumeroVariantiByProduct($product){
        $variantAttribute = Mage::getStoreConfig('magentocollectionblock/config/variant_attribute');
        if($variantAttribute){
            $valueAttribute = $product->getData(Mage::getStoreConfig('magentocollectionblock/config/variant_attribute'));
            if($valueAttribute){
                  if($attribute = Mage::getModel('eav/entity_attribute')->loadByCode(Mage::getModel('catalog/product')->getResource()->getTypeId(),$variantAttribute)){

                      $collection = Mage::getSingleton('bitbull_magentocollectionblock/numeroVarianti',array($attribute->getId()));
                      $numeroVarianti = (int) $collection->getNumeroVariantiByValue($valueAttribute);
                      if($numeroVarianti > 1){
                          return $numeroVarianti;
                      }
                  }
              }
        }
        return false;

    }

} 