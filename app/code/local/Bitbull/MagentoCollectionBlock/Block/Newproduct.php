<?php
/**
 * @category Bitbull
 * @package  Bitbull_MagentoCollectionBlock
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_MagentoCollectionBlock_Block_Newproduct extends Bitbull_MagentoCollectionBlock_Block_Abstract{

    /**
     * retrieve the collection of products
     * @return Mage_Catalog_Model_Resource_Product_Collection|Varien_Data_Collection
     */
    public function getProductCollection(){

        // retrieves products with news_from_date and news_to_date setted
        $collection = $this->getNewProductCollection();

        if($collection->count()==0){
            //if the collection is empty, retrieves last inserted product in db
            $collection= $this->getLastInserProductCollection();
        }
        return $collection;

    }

    /**
     * Retrieves products with news_from_date and news_to_date setted
     * @return Mage_Catalog_Model_Resource_Product_Collection|Varien_Data_Collection
     */
    public function getNewProductCollection(){
        $todayStartOfDayDate  = Mage::app()->getLocale()->date()
            ->setTime('00:00:00')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $todayEndOfDayDate  = Mage::app()->getLocale()->date()
            ->setTime('23:59:59')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());


        //add select attribute in collection
        if(count($this->_select)){
            /** @var Bitbull_MagentoCollectionBlock_Helper_Data $helper */
            $helper = Mage::helper('bitbull_magentocollectionblock/data');

            $collection =$helper->addAttributesToSelect($collection, $this->_select);
        }


        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToFilter('news_from_date', array('or'=> array(
                0 => array('date' => true, 'to' => $todayEndOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter('news_to_date', array('or'=> array(
                0 => array('date' => true, 'from' => $todayStartOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter(
                array(
                    array('attribute' => 'news_from_date', 'is'=>new Zend_Db_Expr('not null')),
                    array('attribute' => 'news_to_date', 'is'=>new Zend_Db_Expr('not null'))
                )
            )
            ->addAttributeToSort('news_from_date', 'desc')
            ->setPageSize($this->_limit)
            ->setCurPage(1);


        return $collection;
    }

    /**
     * retrieves last inserted product in db
     * @return Mage_Catalog_Model_Resource_Product_Collection|Varien_Data_Collection
     */
    public function getLastInserProductCollection(){
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());


        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToSort('entity_id', 'desc')
            ->setPageSize($this->_limit)
            ->setCurPage(1);



        return $collection;
    }

} 