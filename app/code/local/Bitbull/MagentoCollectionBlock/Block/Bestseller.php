<?php
/**
 * @category Bitbull
 * @package  Bitbull_MagentoCollectionBlock
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_MagentoCollectionBlock_Block_Bestseller extends Bitbull_MagentoCollectionBlock_Block_Abstract{


    protected $_select= array('name', 'price', 'thumbnail', 'small_image');

    public function getProductCollection()
    {
        /** @var Bitbull_MagentoCollectionBlock_Helper_Data $helper */
        $helper = Mage::helper('bitbull_magentocollectionblock/data');

        $collection = Mage::getResourceSingleton('reports/product_collection');
        /** attenzione la query che si genera con addOrderedQty aggiunge la seguente condizione
         * (e.type_id NOT IN ('grouped', 'configurable', 'bundle'))
         * questa non fa visualizzare i dati dei prodotti di tipo configurable, bundle e gruped
         */
//            ->addOrderedQty();
        //effettuo l'addrderedQty rimuovendo (e.type_id NOT IN ('grouped', 'configurable', 'bundle'))
        $helper->addOrderedQty($collection);

        //add select attribute in collection
        if(count($this->_select)){
            if(Mage::helper('catalog/product_flat')->isEnabled()){
                $flatTable = Mage::getResourceSingleton('catalog/product_flat')->getFlatTableName();
                /**
                 * Per ovviare al problema del type_id non in arrey è possibile aggiungere questo per recuperare le info

                //                $collection->getSelect()->join(array ('ft' =>$flatTable),
                //                    'ft.entity_id=order_items.product_id',
                //                    $this->_select
                //                );
                 *
                 */
                $collection->joinTable(array('flat_table'=>$flatTable),'entity_id=entity_id', $this->_select);

            }else{
                $collection = $helper->addAttributesToSelect($collection, $this->_select);
            }
        }

        /**
         * sempre lo stesso problema non aggiunge le info per il website dei prodotti con tipo appartenente all array

        //        $collection->getSelect()->join(
        //            array('product_website'=>'catalog_product_website'),
        //            'product_website.product_id = order_items.product_id',
        //            '*'
        //        );
         *
         */

        //set order to view bestseller
        $collection->setOrder('ordered_qty', 'desc');

//      remove non visibile product
        $visibility = Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds();
        $collection->setVisibility($visibility);

        $collection
            ->addStoreFilter()
            //add limit to collection
            ->setPageSize($this->_limit)
            ->setCurPage(1);

        $this->_collection = $collection;

        return $this->_collection;
    }

} 