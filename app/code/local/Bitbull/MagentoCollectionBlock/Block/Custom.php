<?php
/**
 * @category Bitbull
 * @package  Bitbull_MagentoCollectionBlock
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_MagentoCollectionBlock_Block_Custom extends Bitbull_MagentoCollectionBlock_Block_Abstract{


    protected $_category;
    protected $_attribute= array();

    /**
     * Magento attribute name for variants logic
     */
    protected $_variantAttribute;
    /**
     * Product variants
     */
    protected $_variants;
    /**
     * Exclude current product from product variants collection
     */
    protected $_variantExcludeSelf;

    protected $_forceCurrentCategory;


    /**
     * Set path to retrieve the category id by system configuration
     * @param string $categoryPath
     * @return Bitbull_MagentoCollectionBlock_Block_Custom
     */
    public function setCategoryToFilter($categoryPath)
    {
        $categoryId= Mage::getStoreConfig($categoryPath);
        $_cat = Mage::getModel('catalog/category')->load($categoryId);
        if($_cat->getId()){
            $this->_category = $_cat;
        }
        return $this;
    }


    /**
     * Add an attribute to use by filter collection
     * @param string $name
     * @param string $value
     * @param string $type possibile value: text, yesno
     * @return Bitbull_MagentoCollectionBlock_Block_Custom
     */
    public function addAttributeToFilter($name, $value, $type='text'){
        $this->_attribute[$name]= array(
            'value' => $value,
            'type' => $type);
        return $this;
    }


    /**
     * Set current category filter to yes/no
     *
     * @param bool $force
     *
     * @return $this
     */
    public function forceCurrentCategory($force)
    {
        if (!is_null($force)) {
            $this->_forceCurrentCategory = filter_var($force, FILTER_VALIDATE_BOOLEAN);
        }
        return $this;
    }


    public  function getProductCollection()
    {
        $collection = parent::getProductCollection();
        /** @var Bitbull_MagentoCollectionBlock_Helper_Data $helper */
        $helper = Mage::helper('bitbull_magentocollectionblock/data');

        //add category filter
        if($this->_category != null){
            $collection->addCategoryFilter($this->_category);
        }

        //add attribute to filter
        if(count ($this->_attribute)>0){
            foreach($this->_attribute as $name => $detail){
                $collection = $helper->addAttributeFilter($collection, $name, $detail);
            }
        }

        //add current category to filter
        if(!is_null($this->_forceCurrentCategory) && $this->_forceCurrentCategory){
            $collection->addCategoryFilter(Mage::registry("current_category"));
        }

        //add variant attribute to filter
        if (!is_null($this->_variantAttribute)) {
            $collection->addFieldToFilter(
                $this->_variantAttribute, $this->_getProductVariants()
            );
            if (!is_null($this->_variantExcludeSelf) && $this->_variantExcludeSelf) {
                $collection->addFieldToFilter("entity_id", array("neq" => $this->_getProduct()->getEntityId()));
            }
        }

        $this->_collection = $collection;

        return $this->_collection;
    }


    /**
     * Set which attribute is to use for variants logic.
     * Decide whether to exclude current product from variants collection
     *
     * @param $attributeName
     * @param $excludeSelf
     *
     * @return $this
     */
    public function setVariantAttribute($attributeName, $excludeSelf)
    {
        if (!is_null($attributeName)) {
            $this->_variantAttribute = $attributeName;
        } else {
            Mage::log("Invalid attribute for product variants: {$attributeName}");
        }
        if (!is_null($excludeSelf)) {
            $this->_variantExcludeSelf = filter_var($excludeSelf, FILTER_VALIDATE_BOOLEAN);
        }
        return $this;
    }


    /**
     * Get current product
     *
     */
    protected function _getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', Mage::registry('product'));
        }
        return $this->getData('product');
    }


    /**
     * Get current product variants
     *
     * @return array|mixed
     */
    protected function _getProductVariants()
    {
        if (!isset($this->_variants)) {
            $productVariants = $this->_getProduct()->getData(
                $this->_variantAttribute
            );
            if (!is_null($productVariants)) {
                $this->_variants = array($productVariants);
            }
        }
        return $this->_variants;
    }


    /**
     * Get category which the custom filter is applied
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCollectionCategory()
    {
        return $this->_category;
    }


}