Bitbull MagentoCollectionBlock Extension
=====================
Modulo per la creazioni di blocchi di prodotti utilizzando i filtri su attributi e categorie

Facts
-----
- version: 1.0.0
- extension key: Bitbull_MagentoCollectionBlock
- [extension on Bitbucket](https://bitbucket.org/bitbull/magento-collection-block)

Descrizione
-----------
Il modulo permette di inserire all'interno dei file di layout, dei blocchi per recuperare collezioni di prodotti,
In particolare :

- prodotti più venduti
- prodotti filtrati su attributi e categorie
- prodotti "novità"

Prodotti filtrati
-----------
Il blocco Bitbull_MagentoCollectionBlock_Block_Custom permette di filtrare i prodotti tramite i seguenti metodi:

- setCategoryToFilter, dove è possibile inserire il path presente all'interno del system.xml che conterrà un id categoria
- addAttributeToFilter, per aggiungere un filtro sull'attributo.
- setOrder, per impostare un'ordinamento sulla collection. Di default la collection viene ordinata in base al "entity_id"
  con position "DESC"



Esempio d'uso per filtrare su id categoria:

    <block type="bitbull_magentocollectionblock/custom" template="bitbull/collection.phtml" name="bitbull.cat1">
        <action method="setCategoryToFilter">
            <categoryPath>magentocollectionblock/categories/category_1</categoryPath>
        </action>
        <action method="enableRandomProduct"/>
    </block>

Esempio d'uso per filtrare su attributo ( l'attributo deve avere "usato per la lista prodotti: si" ):

    <block type="bitbull_magentocollectionblock/custom" template="catalog/product/list_is_home.phtml" name="list.home">
        <action method="addAttributeToFilter">
            <name>iam_is_home</name>
            <value>4</value>
        </action>
        <action method="setColumnCount"><count>5</count></action>
        <action method="setLimit"><count>5</count></action>
        <action method="addAttributeToSelect"><name>testmirko</name></action>
        <action method="enableRandomProduct"/>
        <action method="setOrder">
            <order>position</order>
            <direction>asc</direction>
        </action>
    </block>


Esempio d'uso per filtrare in base al contenuto di un'attributo (varianti)

	<block type="bitbull_magentocollectionblock/custom" template="catalog/product/list/variants.phtml"
                   name="product.variants">
                <action method="setVariantAttribute">
                    <name>mc_variant</name>
                    <excludeSelf>false</excludeSelf>
                </action>
            </block>

 - nel campo "name" inserire il nome dell'attributo per cui si vuole filtrare
 - nel campo "excludeSelf" inserire true se si vuole escludere il prodotto corrente dalla collection, altrimenti inserire false



Copyright
---------
(c) 2014 Bitbull